package com.back_app.my.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table
@RequiredArgsConstructor
@Getter
@Setter
public class Articulo{

    @Id
    private Long id;

    private String nombre;

    private String descripcion;

    private String precio;
    @Column(nullable = false,columnDefinition = "TIMESTAMP")
    private LocalDateTime fecha_creacion;


}

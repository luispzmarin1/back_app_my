package com.back_app.my.controller;

import com.back_app.my.model.Articulo;
import com.back_app.my.service.ArticuloService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@RestController
@RequestMapping("/art")
@RequiredArgsConstructor
public class ArticuloController {
    private final ArticuloService articuloService;
    private static final Logger logger = LogManager.getLogger(ArticuloController.class);


    @GetMapping("/{id}")
    public ResponseEntity getArt(@PathVariable("id") Long id){
        logger.error("LLEGA");
        Articulo art = articuloService.getArt(id);
        if(art != null){
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(art, HttpStatus.OK);
        }
    }

    @PostMapping("/ta")
    public ResponseEntity getArte(){
        logger.error("LLEGA");
        Articulo art = articuloService.getArt(1L);
        if(art != null){
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }else {
            return new ResponseEntity(art, HttpStatus.OK);
        }
    }


}

package com.back_app.my.service;

import com.back_app.my.model.Articulo;
import com.back_app.my.repository.ArticuloRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ArticuloServiceImp implements ArticuloService{

    private ArticuloRepository articuloRepository;
    @Override
    public Articulo getArt(Long id) {
        Optional<Articulo> art= articuloRepository.findById(id);
        return art.orElse(null);
    }
}

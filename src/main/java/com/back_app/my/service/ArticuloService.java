package com.back_app.my.service;

import com.back_app.my.model.Articulo;

public interface ArticuloService {
    Articulo getArt(Long id);
}

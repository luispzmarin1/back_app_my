CREATE TABLE articulo (
      id SERIAL PRIMARY KEY,
      nombre VARCHAR(50) NOT NULL,
      descripcion TEXT,
      precio NUMERIC(10, 2) NOT NULL,
      fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

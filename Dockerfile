FROM openjdk:18
# Custom cache invalidation
ARG CACHEBUST=1
ADD target/back_app_my-0.0.1.jar /developments/
ENTRYPOINT ["java","-jar","/developments/back_app_my-0.0.1.jar"]